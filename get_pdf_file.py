# -*- coding: utf-8 -*-
"""
Created on Fri Dec 14 15:45:39 2018

@author: GAS01409
"""

import os
import requests
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import WebDriverException
import logging as LOGGER

LOGGER.basicConfig(level=LOGGER.INFO)

###selenium stuff
def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    base_path = os.getcwd()
    return os.path.join(base_path, relative_path)

def initiate_driver():
    global driver
    global wait
    LOGGER.info('initiating driver...')
    try:
        # Set parameters for webdriver
        window_size = "1080,1080"
        chrome_options = Options()
        #chrome_options.add_argument("--headless")  # makes chrome invisible; NO DOWNLOADS ALLOWED
        chrome_options.add_argument("--window-size=%s" % window_size)
        chrome_options.add_argument('--ignore-certificate-errors')
        chrome_options.add_argument('log-level=3')
        chromedriver = resource_path("chromedriver.exe")
        #initiate webdriver
        driver = webdriver.Chrome(executable_path=chromedriver,
                                  chrome_options=chrome_options)
        LOGGER.info('driver initiated')
        wait = WebDriverWait(driver, 300)
    except:
        LOGGER.error('driver failed to initiate')

def login():
    usr = input('enter username')#'alec.kaye@gas-south.com'
    pswd = input('enter pswd')
    try:
        ## go to login page
        driver.get('https://eneract.aglc.com')
        LOGGER.info('logging in %s', usr)
        wait.until(EC.presence_of_element_located((By.NAME,'user_name')))
        login_user = driver.find_element_by_name('user_name')
        login_user.send_keys(usr)
        ## enter password
        login_pw = driver.find_element_by_name('password')
        login_pw.send_keys(pswd, Keys.ENTER)

        wait.until(EC.presence_of_element_located((By.ID,'dvLandingContainer')))
        LOGGER.info('logged in')
    except ImportError:
        LOGGER.warning('authentication failed')

def request_data(url):
    # retrieve the cookies created from browser session
    LOGGER.info('retrieving cookies')
    try:
        cookie = driver.get_cookies()
    except WebDriverException:
        LOGGER.error('no cookies found: driver not available')
        return requests.models.Response()
    
    # making a persistent connection using the requests library
    LOGGER.info('creating session')
    s = requests.Session()
    
    # passing the cookies generated from the browser to the session
    [s.cookies.set(c['name'], c['value']) for c in cookie]

    # getting the file content
    LOGGER.info('retrieving file content')
    r = s.get(url)
    LOGGER.info('retrieved')

    return r

def save_data(r, filename):
    if r.status_code != 200:
        LOGGER.warning('response unsuccessful')
    else:
        LOGGER.info('saving file...')
        with open(filename, 'wb') as output:
            output.write(r.content)
        LOGGER.info('saved %s', filename)

if __name__ == '__main__':
    initiate_driver()
    login()
    url = 'https://eneract.aglc.com/GOS/Download?Filename=238003319_id-trans-wksht_201811.pdf'
    data = request_data(url)
    save_data(data, filename='test.pdf')
    driver.close()
